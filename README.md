## Processus de positionnement backend project
This project is a web application for the positioning of consultants

contains two types of account with usernames and passwords

+ a Super-Admin account (Commercial Director) which allows you to:

  - add a new Admin
  - suspend an existing Admin
  - reset the password of an Admin 
  + an Admin (Commercial) account can:
+ Managed all the steps related to the positioning process

### Prerequisites
For building and running the application you need:

- JDK 1.8
- Maven 3

### How to Run
+ Clone this repository
```
$ git clone https://Ahmed_Hssin@bitbucket.org/Ahmed_Hssin/processus-de-positionnement-backend-project.git

```
+ Make sure you are using JDK 1.8 and Maven 3.x
+ You can build the project and run the tests by running mvn clean package
* You can build the project and run the tests by running ```mvn clean package```
* Once successfully built, you can run the service by one of these two methods:
```
        java -jar -Dspring.profiles.active=test target/spring-boot-rest-example-0.5.0.war
or
        mvn spring-boot:run -Drun.arguments="spring.profiles.active=test"
```
* Check the stdout or boot_example.log file to make sure no exceptions are thrown

Once the application runs you should see something like this

```
2021-10-02 16:41:50.976  INFO 15832 --- [  restartedMain] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8080 (http) with context path ''
2021-10-02 16:41:50.984  INFO 15832 --- [  restartedMain] r.p.ProcessusDePositionnementApplication : Started ProcessusDePositionnementApplication in 6.19 seconds (JVM running for 6.619)
```


> :warning: **if you get an error you must import "processus_de_positionnement.sql" file into your local database**

### To get API Documentation (swagger2)

```
http://localhost:8080/v2/api-docs

```
### Create consultant

```
POST http://localhost:8080/consultant/add
Accept: application/json
Content-Type: application/json

{
  "disponibilite": "2021-10-06T09:10:39.044Z",
  "id": 0,
  "mail": "string",
  "mobilite": "string",
  "niveauAnglais": "string",
  "niveauFrancais": "string",
  "nom": "string",
  "numTel": 0,
  "prenom": "string",
  "seniorite": "string",
  "statut": "string",
  "title": "string"
}

RESPONSE: HTTP 201 (Created)

```

### Append this to the end of application.yml:

```
jwt:
  secret: '[a-zA-Z0-9._]^+$eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ$'
spring:
  datasource:
    url: jdbc:mysql://<your_mysql_host_or_ip>/bootexample
    username: <your_mysql_username>
    password: <your_mysql_password>
  jpa:
    hibernate:
      ddl-auto: update
    properties:
      hibernate:
        dialect: org.hibernate.dialect.MySQL5Dialect
    generate-ddl : true
```

## Built With
- __[Spring Boot](https://spring.io/projects/spring-boot)__ - Spring Boot est un framework de d�veloppement JAVA
- __[Spring security](https://spring.io/projects/spring-security)__ - Internationalized and accessible components for everyone.
- __[JWT](https://jwt.io/)__ - JSON Web Token (JWT)
