-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : dim. 10 oct. 2021 à 23:52
-- Version du serveur : 10.4.21-MariaDB
-- Version de PHP : 8.0.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `processus_de_positionnement`
--

-- --------------------------------------------------------

--
-- Structure de la table `briefing`
--

CREATE TABLE `briefing` (
  `id_briefing` bigint(20) NOT NULL,
  `date_briefing` datetime DEFAULT NULL,
  `dure` varchar(255) DEFAULT NULL,
  `remarque` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `consultant_id` bigint(20) DEFAULT NULL,
  `cv_envoyee_id` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `briefing_contact`
--

CREATE TABLE `briefing_contact` (
  `briefing_id` bigint(20) NOT NULL,
  `contact_id` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

CREATE TABLE `client` (
  `idclient` bigint(20) NOT NULL,
  `datedenvoi` datetime DEFAULT NULL,
  `descriptif` varchar(255) DEFAULT NULL,
  `dure_mission` datetime DEFAULT NULL,
  `intituleposte` varchar(255) DEFAULT NULL,
  `lieu_mission` varchar(255) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `secteuractivite` varchar(255) DEFAULT NULL,
  `tjm` float NOT NULL,
  `prenom` varchar(255) DEFAULT NULL,
  `remarques` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `consultant`
--

CREATE TABLE `consultant` (
  `id` bigint(20) NOT NULL,
  `mobilite` varchar(255) DEFAULT NULL,
  `seniorite` varchar(255) DEFAULT NULL,
  `consultant_code` varchar(255) NOT NULL,
  `disponibilite` date DEFAULT NULL,
  `mail` varchar(255) DEFAULT NULL,
  `niveau_anglais` varchar(255) DEFAULT NULL,
  `niveau_francais` varchar(255) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `num_tel` bigint(20) NOT NULL,
  `prenom` varchar(255) DEFAULT NULL,
  `statut` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `consultant`
--

INSERT INTO `consultant` (`id`, `mobilite`, `seniorite`, `consultant_code`, `disponibilite`, `mail`, `niveau_anglais`, `niveau_francais`, `nom`, `num_tel`, `prenom`, `statut`, `title`) VALUES
(65, 'Toute la france', 'Débutant', '08a5c6d8-f755-4a90-bb4b-150357c1bd39', '2021-09-15', 'sabrine@gmail.com', 'Débutant', 'Intermédiaire', 'Trabelsi', 654644151, 'Sabrine', 'En mission', 'product owner'),
(66, 'IDF', 'Débutant', '7e6b45a7-6414-4152-91e6-3f59f5e8c8b9', '2021-10-10', 'mohamed@gmail.com', 'Intermédiaire', 'Intermédiaire', 'Ali', 654644151, 'Mohamed', 'Intercontrat', 'product owner'),
(67, 'IDF', 'Junior', 'a87451ec-6465-4a23-8ab2-c6cf6ef848fe', '2021-09-23', 'ahmedhassine2017@gmail.com', 'Intermédiaire', 'Débutant', 'Hassine', 54548485, 'Ahmed', 'Sortie', 'Spring devloper'),
(69, 'Toute la france', 'Confirmé', 'b5a561dd-8652-4575-8d4a-4ab27737ff42', '2021-10-31', '', NULL, NULL, 'ipsum', 0, 'lorem', 'Sortie', 'dev');

-- --------------------------------------------------------

--
-- Structure de la table `contact`
--

CREATE TABLE `contact` (
  `id_contact` bigint(20) NOT NULL,
  `mail` varchar(255) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `prenom` varchar(255) DEFAULT NULL,
  `telephone` bigint(20) DEFAULT NULL,
  `poste` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `contact`
--

INSERT INTO `contact` (`id_contact`, `mail`, `nom`, `prenom`, `telephone`, `poste`) VALUES
(40, 'mail@gmail.tn', 'nom', 'Prenom', 54548485, 'poste');

-- --------------------------------------------------------

--
-- Structure de la table `cvenvoye_contact`
--

CREATE TABLE `cvenvoye_contact` (
  `cvenvoye_id` bigint(20) NOT NULL,
  `contact_id` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `cv_envoyee`
--

CREATE TABLE `cv_envoyee` (
  `idcv` bigint(20) NOT NULL,
  `nom_societe` varchar(255) DEFAULT NULL,
  `partenair_client` varchar(255) DEFAULT NULL,
  `statut` varchar(255) DEFAULT NULL,
  `tjm` float NOT NULL,
  `date_envoi` datetime DEFAULT NULL,
  `remarques` varchar(255) DEFAULT NULL,
  `consultant_id` bigint(20) DEFAULT NULL,
  `etape_actuel` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `entretien`
--

CREATE TABLE `entretien` (
  `id_entretien` bigint(20) NOT NULL,
  `statut` varchar(255) DEFAULT NULL,
  `tjm` float NOT NULL,
  `type_entretien` varchar(255) DEFAULT NULL,
  `date_entretien` datetime DEFAULT NULL,
  `remarque` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `entretien_client`
--

CREATE TABLE `entretien_client` (
  `id_entretien_client` bigint(20) NOT NULL,
  `tjm` float NOT NULL,
  `type_entretien` varchar(255) DEFAULT NULL,
  `date_entretien` datetime DEFAULT NULL,
  `lieu` varchar(255) DEFAULT NULL,
  `nom_du_client` varchar(255) DEFAULT NULL,
  `remarque` varchar(255) DEFAULT NULL,
  `consultant_id` bigint(20) DEFAULT NULL,
  `cv_envoyee_id` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `entretien_partenaire`
--

CREATE TABLE `entretien_partenaire` (
  `id_entretien` bigint(20) NOT NULL,
  `statut` varchar(255) DEFAULT NULL,
  `tjm` float NOT NULL,
  `type_entretien` varchar(255) DEFAULT NULL,
  `date_entretien` datetime DEFAULT NULL,
  `remarque` varchar(255) DEFAULT NULL,
  `consultant_id` bigint(20) DEFAULT NULL,
  `cv_envoyee_id` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `partenaire`
--

CREATE TABLE `partenaire` (
  `id_partenaire` bigint(20) NOT NULL,
  `mail` varchar(255) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `post` varchar(255) DEFAULT NULL,
  `prenom` varchar(255) DEFAULT NULL,
  `telephone` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `positionnement`
--

CREATE TABLE `positionnement` (
  `id_positionnement` bigint(20) NOT NULL,
  `secteur_activite` varchar(255) DEFAULT NULL,
  `tjm` float NOT NULL,
  `date` datetime DEFAULT NULL,
  `descriptif_de_la_mission` varchar(255) DEFAULT NULL,
  `durede_la_mission` varchar(255) DEFAULT NULL,
  `intitule_du_poste` varchar(255) DEFAULT NULL,
  `nom_du_client` varchar(255) DEFAULT NULL,
  `remarque` varchar(255) DEFAULT NULL,
  `consultant_id` bigint(20) DEFAULT NULL,
  `lieu_de_la_mission` varchar(255) DEFAULT NULL,
  `cv_envoyee_id` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `role`
--

CREATE TABLE `role` (
  `role_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `role`
--

INSERT INTO `role` (`role_name`) VALUES
('Admin'),
('Super_Admin');

-- --------------------------------------------------------

--
-- Structure de la table `test_technique_client`
--

CREATE TABLE `test_technique_client` (
  `id_test_technique_client` bigint(20) NOT NULL,
  `type_entretien` varchar(255) DEFAULT NULL,
  `date_entretien` datetime DEFAULT NULL,
  `observations` varchar(255) DEFAULT NULL,
  `dure` varchar(255) DEFAULT NULL,
  `consultant_id` bigint(20) DEFAULT NULL,
  `cv_envoyee_id` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `user_name` varchar(255) NOT NULL,
  `user_first_name` varchar(255) NOT NULL,
  `user_last_name` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`user_name`, `user_first_name`, `user_last_name`, `user_password`, `email`) VALUES
('admin', 'prenom', 'nom', '$2a$10$ujonNB1ypEcrEK0lkmVeguLUGWryYSgp91GBXbtml673NJO02GtmO', 'email@gmail.com'),
('ahmed', 'nom', 'prenom', '$2a$10$pwrKrperj7lHoFYHITCffO6wcgLnfxU.jXxMQgzCxoG1JBSZ8awGi', 'ahmed@gmail.com'),
('raj123', 'raj', 'sharma', '$2a$10$/zRRxe/7Am8axXIIfVZTd.L/2Th.QLW8dOhYvSn6VUNheRCYyiYki', 'raj@gmail.com'),
('superAdmin123', 'super', 'admin', '$2a$10$kOAl2mLo/efsokQGutsGuOfPALMxkh3p10ukEABT3j/BJut2Aueeq', NULL),
('userName', 'prenom', 'nom', '$2a$10$ryayDDFn6NPtGazR/0qb..R5W0E2ca0l5V4J3ZtteSWRSPRGAucYO', 'prenom@gmail.com');

-- --------------------------------------------------------

--
-- Structure de la table `user_role`
--

CREATE TABLE `user_role` (
  `user_id` varchar(255) NOT NULL,
  `role_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `user_role`
--

INSERT INTO `user_role` (`user_id`, `role_id`) VALUES
('superAdmin123', 'Super_Admin'),
('ahmed', 'Admin'),
('raj123', 'Admin'),
('userName', 'Admin'),
('admin', 'Admin');

-- --------------------------------------------------------

--
-- Structure de la table `validation`
--

CREATE TABLE `validation` (
  `id_validation` bigint(20) NOT NULL,
  `demarrage_mission` varchar(255) DEFAULT NULL,
  `feedback` varchar(255) DEFAULT NULL,
  `mesures_teletravail` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `remarque` varchar(255) DEFAULT NULL,
  `consultant_id` bigint(20) DEFAULT NULL,
  `cv_envoyee_id` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `briefing`
--
ALTER TABLE `briefing`
  ADD PRIMARY KEY (`id_briefing`),
  ADD KEY `FKesgx1ubs44y2t5st7e7q4djh2` (`consultant_id`),
  ADD KEY `FK2ltnr6w5x3w6c03pvdpm0mjnn` (`cv_envoyee_id`);

--
-- Index pour la table `briefing_contact`
--
ALTER TABLE `briefing_contact`
  ADD PRIMARY KEY (`briefing_id`,`contact_id`),
  ADD KEY `FKrret4mtaafy68in0548ejquc0` (`contact_id`);

--
-- Index pour la table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`idclient`);

--
-- Index pour la table `consultant`
--
ALTER TABLE `consultant`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id_contact`);

--
-- Index pour la table `cvenvoye_contact`
--
ALTER TABLE `cvenvoye_contact`
  ADD PRIMARY KEY (`cvenvoye_id`,`contact_id`),
  ADD KEY `FKdccf11eiacgop0jlsmgyldbh0` (`contact_id`);

--
-- Index pour la table `cv_envoyee`
--
ALTER TABLE `cv_envoyee`
  ADD PRIMARY KEY (`idcv`),
  ADD KEY `FKbdfbq1bp6u8uv4tldvnx6ifw5` (`consultant_id`);

--
-- Index pour la table `entretien`
--
ALTER TABLE `entretien`
  ADD PRIMARY KEY (`id_entretien`);

--
-- Index pour la table `entretien_client`
--
ALTER TABLE `entretien_client`
  ADD PRIMARY KEY (`id_entretien_client`),
  ADD KEY `FK6fb298uohsuv4n3guasy8rac8` (`consultant_id`),
  ADD KEY `FKb3yu9w8h340go9m1jutcebjf` (`cv_envoyee_id`);

--
-- Index pour la table `entretien_partenaire`
--
ALTER TABLE `entretien_partenaire`
  ADD PRIMARY KEY (`id_entretien`),
  ADD UNIQUE KEY `UK_q3skih1mqrudp35j3m0dpji70` (`cv_envoyee_id`),
  ADD KEY `FK557w04htlxlswh6w16hubkapq` (`consultant_id`);

--
-- Index pour la table `partenaire`
--
ALTER TABLE `partenaire`
  ADD PRIMARY KEY (`id_partenaire`);

--
-- Index pour la table `positionnement`
--
ALTER TABLE `positionnement`
  ADD PRIMARY KEY (`id_positionnement`),
  ADD KEY `FKbcndpe2syputkx2om7t5e249t` (`consultant_id`),
  ADD KEY `FK7q7sxw4qp4dn7rh1dmpuvexyp` (`cv_envoyee_id`);

--
-- Index pour la table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`role_name`);

--
-- Index pour la table `test_technique_client`
--
ALTER TABLE `test_technique_client`
  ADD PRIMARY KEY (`id_test_technique_client`),
  ADD KEY `FKwmyx30midorqvend3fdhoi3c` (`consultant_id`),
  ADD KEY `FKc3b0y79at070jcoxq2n97mhxi` (`cv_envoyee_id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_name`);

--
-- Index pour la table `user_role`
--
ALTER TABLE `user_role`
  ADD KEY `FKa68196081fvovjhkek5m97n3y` (`role_id`),
  ADD KEY `FK859n2jvi8ivhui0rl0esws6o` (`user_id`);

--
-- Index pour la table `validation`
--
ALTER TABLE `validation`
  ADD PRIMARY KEY (`id_validation`),
  ADD KEY `FKnfrllqpdpyer1jvl92onua0ad` (`consultant_id`),
  ADD KEY `FK5sm9lg358330gdta8d9363msd` (`cv_envoyee_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `briefing`
--
ALTER TABLE `briefing`
  MODIFY `id_briefing` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `client`
--
ALTER TABLE `client`
  MODIFY `idclient` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `consultant`
--
ALTER TABLE `consultant`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT pour la table `contact`
--
ALTER TABLE `contact`
  MODIFY `id_contact` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT pour la table `cv_envoyee`
--
ALTER TABLE `cv_envoyee`
  MODIFY `idcv` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=422;

--
-- AUTO_INCREMENT pour la table `entretien`
--
ALTER TABLE `entretien`
  MODIFY `id_entretien` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `entretien_client`
--
ALTER TABLE `entretien_client`
  MODIFY `id_entretien_client` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT pour la table `entretien_partenaire`
--
ALTER TABLE `entretien_partenaire`
  MODIFY `id_entretien` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT pour la table `partenaire`
--
ALTER TABLE `partenaire`
  MODIFY `id_partenaire` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `positionnement`
--
ALTER TABLE `positionnement`
  MODIFY `id_positionnement` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT pour la table `test_technique_client`
--
ALTER TABLE `test_technique_client`
  MODIFY `id_test_technique_client` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `validation`
--
ALTER TABLE `validation`
  MODIFY `id_validation` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `user_role`
--
ALTER TABLE `user_role`
  ADD CONSTRAINT `FK859n2jvi8ivhui0rl0esws6o` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_name`),
  ADD CONSTRAINT `FKa68196081fvovjhkek5m97n3y` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_name`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
